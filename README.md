# Node forkit

Execute any function without blocking the main thread.

## Installation

```bash
npm -i node-forkit
```

## Usage

```js
const forkIt = require('node-forkit');

const my_function = () => {
    // long function.
    let duration = 60
    let start = new Date().getTime()
    let expire = start + 1000 * duration;
    while (new Date().getTime() < expire) {}

    console.log(`Executed in a forked process and took {duration}s.`);
};

await forkIt(my_function);
    .catch(err => console.error(err));
// Executed in a forked process and took 60s.
```

```js
const forkIt = require('node-forkit');

let data = await forkit(__dirname + '/path_to_file', {...arguments})
    .catch(err => console.error(err)); 
// 7
 ```

## License
[MIT](https://choosealicense.com/licenses/mit/)