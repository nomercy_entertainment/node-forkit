process.on('message', async ({data, args}) => {
  let func;

  if(data.includes('(') || data.includes('function')){
    func = eval(`(${data})`);
  }
  else{  
    func = require(data);
  }

  let res = {
    response: undefined,
    error: undefined,
  };

  if(typeof func === 'function'){
    try {
      res.response = await func(args);
    } catch (error) {
      res.error = error;
    }
  }
  else{
    res.error = 'func is not a valid function.'
  }

  process.send(res);
  process.exit(0);
});