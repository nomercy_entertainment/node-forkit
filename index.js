const { fork } = require('child_process');

/**
 * @description Spawns a new thread and executes the function.
 * @param  {Function} data 
 * @param  {Object} args
 * @example 
 * const my_function = () => {
 *    // do some hard work.
 *    return "Executed in a forked process and took a very long to time.";
 * };
 * let data = await forkIt(my_function).catch(err => console.error(err)); // 'Executed in a forked process and took a very long to time.'
 * 
 * @example
 * let data = await forkit(__dirname + '/path_to_file', {...arguments}).catch(err => console.error(err)); // 7
 * @author Stoney_Eagle <stoney@nomercy.tv>
 */
const forkit = (data, args = {}) => {
  return new Promise(function (resolve, reject) {

    const childProcess = fork(__dirname + '/fork', {
      windowsHide: true,
    });

    childProcess.send({data: data.toString(), args});

    childProcess.on('message', (res) => {

      if (!res.error) return resolve(res.response);

      return reject(res.error);
    });
  })
}

module.exports = forkit;